package priv.pweso.socialdemo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.pweso.socialdemo.config.Constants;
import priv.pweso.socialdemo.domain.Person;
import priv.pweso.socialdemo.model.ChangePassword;
import priv.pweso.socialdemo.model.PersonView;
import priv.pweso.socialdemo.model.SignUp;
import priv.pweso.socialdemo.security.CurrentProfile;
import priv.pweso.socialdemo.service.PersonService;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(value = Constants.URI_API_PREFIX)
@RequiredArgsConstructor
@Slf4j
public class ProfileController {

    private final PersonService personService;

    @GetMapping("/login")
    public ResponseEntity<PersonView> login(@CurrentProfile Person profile) {
        log.debug("REST request to get current profile: {}", profile);

        if (null == profile) {
            log.warn("Attempt getting unauthorised profile information failed");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        log.warn("Profile: {} ", profile);
        return ResponseEntity.ok(new PersonView(profile));
    }

    @PostMapping("/signUp")
    public ResponseEntity<String> signUp(@Valid @RequestBody SignUp profile) {
        log.debug("REST request to sign up a new profile: {}", profile);

        final Person result = personService.findByEmail(profile.getUserName());
        if (null != result) {
            log.debug("Attempt sign up email: {} failed! E-mail is already user by another contact: {}",
                    profile.getUserName(), result);

            return ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body(Constants.ERROR_SIGN_UP_EMAIL);
        }

        final Person newProfile = personService.create(profile);

        URI uri = null;
        try {
            uri = new URI(Constants.URI_API_PREFIX + "/person/" + newProfile.getId());
        } catch (URISyntaxException e) {
            log.debug("Failed creating new URI object with Id from profile: {} with exception message: {}",
                    newProfile, e.getMessage());
        }

        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/changePassword")
    public ResponseEntity changePassword(
            @AuthenticationPrincipal Person profile,
            @Valid @RequestBody ChangePassword pwd) {
        log.debug("REST request to change pwd: {}", pwd);
        if (null == profile) {
            log.warn("Attempt to change unathorised profile password failed!");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        final String currentPassword = pwd.getCurrentPassword();
        final String newPassword = pwd.getPassword();
        if (!personService.hasValidPassword(profile, currentPassword)) {
            log.warn("Current password: {} does not match profile's one: {}", currentPassword, profile);
            return ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body(Constants.ERROR_PASSWORD_CONFIRMATION);
        }

        personService.changePassword(profile, newPassword);

        return ResponseEntity.ok().build();
    }
}
