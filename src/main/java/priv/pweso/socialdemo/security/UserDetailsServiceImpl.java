package priv.pweso.socialdemo.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import priv.pweso.socialdemo.domain.Person;
import priv.pweso.socialdemo.repository.PersonRepository;

@Component
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private final PersonRepository personRepository;

    public UserDetailsServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.info("Authentication request: {}", email);

        Person profile = personRepository.findByEmail(email);
        if (profile == null) {
            throw  new UsernameNotFoundException("Profile is not found: " + email);
        }

        log.info("Profile was found by e-mail: {}", email);

        return profile;
    }
}
