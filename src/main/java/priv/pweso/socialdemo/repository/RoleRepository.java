package priv.pweso.socialdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import priv.pweso.socialdemo.domain.Role;

import java.util.Set;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Set<Role> findAll();

    Role findByName(String name);
}
