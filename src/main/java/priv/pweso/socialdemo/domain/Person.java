package priv.pweso.socialdemo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import org.hibernate.annotations.Formula;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import priv.pweso.socialdemo.domain.jpa.GenderConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@EqualsAndHashCode(of = {"email"})
@ToString(of = {"id", "fullName"})
public class Person implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String firstName;

    @Column(length = 50)
    private String lastName;

    @Column(insertable = false)
    @Formula(value = "concat(first_name, ' ', last_name)")
    private String fullName;

    private String shortName;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(length = 60, nullable = false)
    private String password;

    @Column(length = 15)
    private String phone;

    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @Convert(converter = GenderConverter.class)
    private Gender gender = Gender.UNDEFINED;

    @Column(updatable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created = new Date();

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "friends",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "friend_id"))
    private Set<Person> friends = new HashSet<>();

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "friends",
            joinColumns = @JoinColumn(name = "friend_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id"))
    private Set<Person> friendOf = new HashSet<>();


    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public boolean hasFriend(Person friend) {
        return friends.contains(friend);
    }

    public void addFriend(Person friend) {
        friends.add(friend);
        friend.friendOf.add(this);
    }

    public void removeFriend(Person friend) {
        friends.remove(friend);
        friend.friendOf.remove(this);
    }

    public boolean isFriendOf(Person person) {
        return friendOf.contains(person);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        setFullName();
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        setFullName();
    }

    public void setFullName() {
        this.fullName = String.format("%s %s", firstName, lastName);
    }

    public Person(Long id, String firstName, String lastName, String shortName, String email, String password,
                  String phone, Date birthDate, Gender gender, Set<Role> roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.shortName = shortName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.birthDate = birthDate;
        this.gender = gender;
        this.roles = roles;
        setFullName();
    }

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return getRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @ToString(of = {"id", "firstName", "lastName"})
    public static class PersonBuilder {
        private Long id;
        private String firstName = "";
        private String lastName = "";
        private String shortName = "";
        private String email = "";
        private String password = "";
        private String phone = "";
        private Date birthDate;
        private Gender gender = Gender.UNDEFINED;
        private Set<Role> roles = Collections.singleton(Role.USER);

        public PersonBuilder id(@NonNull Long id) {
            this.id = id;
            return this;
        }

        public PersonBuilder firstName(@NonNull String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PersonBuilder lastName(@NonNull String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PersonBuilder shortName(String shortName) {
            this.shortName = shortName;
            return this;
        }

        public PersonBuilder email(@NonNull String email) {
            this.email = email;
            return this;
        }

        public PersonBuilder password(@NonNull String password) {
            this.password = password;
            return this;
        }

        public PersonBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public PersonBuilder birthDate(Date birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonBuilder gender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public PersonBuilder roles(Set<Role> roles) {
            this.roles = roles;
            return this;
        }

        public Person build() {
            return new Person(id, firstName, lastName, shortName, email, password, phone, birthDate, gender, roles);
        }
    }
}
