package priv.pweso.socialdemo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.pweso.socialdemo.domain.Person;
import priv.pweso.socialdemo.model.SignUp;
import priv.pweso.socialdemo.repository.PersonRepository;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PasswordEncoder passwordEncoder;
    private final PersonRepository personRepository;

    @Transactional(readOnly = true)
    public Person findById(Long id) {
        return personRepository.findById(id).get();
    }

    @Transactional(readOnly = true)
    public Person findByEmail(String email) {
        return personRepository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    public Page<Person> getPeople(String searchTerm, Pageable pageRequest) {
        return personRepository.findPeople(searchTerm, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<Person> getFriends(Person person, String searchTerm, Pageable pageRequest) {
        return personRepository.findFriends(person, searchTerm, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<Person> getFriendOf(Person person, String searchTerm, Pageable pageRequest) {
        return personRepository.findFriendOf(person, searchTerm, pageRequest);
    }

    @Transactional
    public void addFriend(Person person, Person friend) {
        if (!person.hasFriend(friend)) {
            person.addFriend(friend);
        }
    }

    @Transactional
    public void removeFriend(Person person, Person friend) {
        if (person.hasFriend(friend)) {
            person.removeFriend(friend);
        }
    }

    @Transactional
    public void update(Person person) {
        personRepository.save(person);
    }

    public Person create(SignUp profile) {
        final Person person = Person.builder()
                .firstName(profile.getFirstName())
                .lastName(profile.getLastName())
                .email(profile.getUserName())
                .password(passwordEncoder.encode(profile.getPassword()))
                .build();
        return personRepository.save(person);
    }


    public boolean hasValidPassword(Person person, String pwd) {
        return passwordEncoder.matches(pwd, person.getPassword());
    }

    public void changePassword(Person person, String pwd) {
        person.setPassword(passwordEncoder.encode(pwd));
        personRepository.save(person);
    }
}
