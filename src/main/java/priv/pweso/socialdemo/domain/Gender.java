package priv.pweso.socialdemo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public enum  Gender implements Serializable {
    UNDEFINED(0),
    MALE(1),
    FEMALE(2);

    private int id;

    public static Gender getGender(Integer id) {
        if (id == null) {
            return null;
        }

        for (Gender g : values()) {
            if (g.getId() == id) {
                return g;
            }
        }
        return null;
    }
}
