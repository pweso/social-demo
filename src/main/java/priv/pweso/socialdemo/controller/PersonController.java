package priv.pweso.socialdemo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.pweso.socialdemo.config.Constants;
import priv.pweso.socialdemo.domain.Person;
import priv.pweso.socialdemo.model.PersonView;
import priv.pweso.socialdemo.repository.PersonRepository;
import priv.pweso.socialdemo.security.CurrentProfile;
import priv.pweso.socialdemo.service.PersonService;

@RestController
@RequestMapping(value = Constants.URI_API_PREFIX)
@RequiredArgsConstructor
@Slf4j
public class PersonController {
    private final PersonService personService;

    @GetMapping("/person/{id}")
    public ResponseEntity<PersonView> getPerson(@PathVariable Long id) {
        log.debug("REST request to get persion id: {}", id);

        Person person = personService.findById(id);
        if (null == person) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new PersonView(person));
    }

    @GetMapping("/people")
    public Page<PersonView> getPeople(
            @RequestParam(defaultValue = "", required = false) String searchTerm,
            @PageableDefault Pageable pageRequest) {
        log.debug("REST request to get people list (searchTerm:{}, pageRequest:{})", searchTerm, pageRequest);

        Page<Person> people = personService.getPeople(searchTerm, pageRequest);

        return people.map(PersonView::new);
    }

    @GetMapping("/friends")
    public Page<PersonView> getFriends(
            @CurrentProfile Person profile,
            @RequestParam(defaultValue = "", required = false) String searchTerm,
            @PageableDefault Pageable pageRequest) {
        log.debug("REST request to get person's: {} friend list (searchTerm: {}, pageRequest: {})", profile, searchTerm, pageRequest);

        Page<Person> friends = personService.getFriends(profile, searchTerm, pageRequest);

        return friends.map(PersonView::new);
    }

    @GetMapping("/friendOf")
    public Page<PersonView> getFriendOf(
            @CurrentProfile Person profile,
            @RequestParam(defaultValue = "", required = false) String searchTerm,
            @PageableDefault Pageable pageRequest) {
        log.debug("REST request to get person's: {} friend_of list (searchTerm:{}, pageRequest:{})", profile, searchTerm, pageRequest);

        Page<Person> friendOf = personService.getFriendOf(profile, searchTerm, pageRequest);
        return friendOf.map(PersonView::new);
    }

    @PutMapping("/friends/add/{personId}")
    public ResponseEntity<Void> addFriend(
            @CurrentProfile Person profile,
            @PathVariable Long personId) {
        log.debug("REST request to add id: {} as a person's: {} friend", personId, profile);

        Person person = personService.findById(personId);
        if (null == person) {
            return ResponseEntity.notFound().build();
        }

        personService.addFriend(profile, person);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/friends/remove/{personId}")
    public ResponseEntity<Void> removeFriend(
            @CurrentProfile Person profile,
            @PathVariable Long personId) {
        log.debug("REST request to remove id: {} as a person's: {} friend", personId, profile);

        Person person = personService.findById(personId);
        if (null == person) {
            return ResponseEntity.notFound().build();
        }

        personService.removeFriend(profile, person);

        return ResponseEntity.ok().build();
    }
}
