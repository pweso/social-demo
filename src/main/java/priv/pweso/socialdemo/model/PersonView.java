package priv.pweso.socialdemo.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import priv.pweso.socialdemo.domain.Person;
import priv.pweso.socialdemo.security.SecurityUtils;

import java.io.Serializable;
import java.util.Date;

@Getter
@ToString
@Slf4j
public class PersonView implements Serializable {


    private Long id;
    private String firstName;
    private String lastName;
    private String fullName;
    private String shortName;
    private String email;
    private String phone;
    private Date birthDate;
    private String gender;
    private Date created;
    private boolean isMyFriend;
    private boolean isFriendOfMine;

    public PersonView(Person person) {
        final Person profile = SecurityUtils.currentProfile();

        log.warn("{}", profile.getFriends());
//        this.id = person.getId();
//        this.firstName = person.getFirstName();
//        this.lastName = person.getLastName();
//        this.fullName = person.getFullName();
//        this.shortName = person.getShortName();
//        this.email = person.getEmail();
//        this.phone = person.getPhone();
//        this.birthDate = person.getBirthDate();
//        this.gender = person.getGender().toString();
//        this.created = person.getCreated();
//        this.isMyFriend = person.isFriendOf(profile);
//        this.isFriendOfMine = person.hasFriend(profile);
    }

}