package priv.pweso.socialdemo.config;

public final class Constants {


    public static final String URI_API_PREFIX = "/api";
    public static final String URI_MESSAGES = URI_API_PREFIX + "/messages";
    public static final String URI_SOCIAL = URI_API_PREFIX + "/social";

    public static final String ERROR_UPDATE_PROFILE = "Updating profile doesn't match the current one";
    public static final String ERROR_UPDATE_EMAIL = "E-mail is already used by another person";
    public static final String ERROR_SIGN_UP_EMAIL = ERROR_UPDATE_EMAIL;
    public static final String ERROR_PASSWORD_CONFIRMATION = "Current password is invalid";

    public static String API_URL;
    public static String WEB_URL;
    public static String AVATAR_FOLDER;

    static String REMEMBER_ME_TOKEN;
    static String REMEMBER_ME_COOKIE;

    static final String XSRF_TOKEN_COOKIE_NAME = "XSRF-TOKEN";
    static final String XSRF_TOKEN_HEADER_NAME = "X-XSRF-TOKEN";
}
